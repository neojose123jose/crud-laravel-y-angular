import { TestBed } from '@angular/core/testing';

import { AplicacionessService } from './aplicacioness.service';

describe('AplicacionessService', () => {
  let service: AplicacionessService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AplicacionessService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
