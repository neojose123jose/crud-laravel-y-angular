export interface Aplicaciones{
    id?: number;
    nombres: string;
    apellidos: string;
    correo: string;
    telefono: number;
    direccion: string;
    created_at?: string;
    update_at?: string;
}