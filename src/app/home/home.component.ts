import { Component, OnInit } from '@angular/core';
import {AplicacionessService} from '../services/aplicacioness.service';
import {HttpClient} from '@angular/common/http';
import {Aplicaciones} from '../interfaces/aplicaciones';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  API_ENDPOINT = 'http://localhost:8000/app';
  app: Aplicaciones[];
  constructor(private aplicaciones: AplicacionessService, private httpClient: HttpClient) { 
    httpClient.get(this.API_ENDPOINT).subscribe((data: Aplicaciones[])=>{
      this.app = data;
    });
  }

  ngOnInit(): void {
  }

}
