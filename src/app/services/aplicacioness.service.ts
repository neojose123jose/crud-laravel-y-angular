import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Aplicaciones} from '../interfaces/aplicaciones';
@Injectable({
  providedIn: 'root'
})
export class AplicacionessService {
  API_ENDPOINT = 'http://localhost:8000/app';

  constructor(private httpClient: HttpClient) { }
  save(app: Aplicaciones){
    const  header = new  HttpHeaders({'Content-ty': 'application/json'});
    return this.httpClient.post(this.API_ENDPOINT, app, {headers: header});
  }
}
