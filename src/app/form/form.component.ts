import { Component, OnInit } from '@angular/core';
import {AplicacionessService} from '../services/aplicacioness.service';
import {HttpClient} from '@angular/common/http';
import {Aplicaciones} from '../interfaces/aplicaciones';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
   app: Aplicaciones = {
    nombres: null,
    apellidos: null,
    correo: null,
    telefono: null,
    direccion: null
  }
  constructor(private  applicaciones: AplicacionessService) { }

  ngOnInit(): void {
  }

  
  save(){
    this.applicaciones.save(this.app).subscribe((data) => {
      alert('pelicula guardada');
      console.log(data);
    }, (error) => {
      alert('ocurrio algo');
    });
  }

}
